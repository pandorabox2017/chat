const fs = require('fs');
const http = require('http');
const path = require('path');
const logger = require('morgan');
const express = require('express');
const socket = require('socket.io');
const { v4: uuidv4 } = require('uuid');
const compress = require('compression');
const bodyParser = require('body-parser');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const redis = require('redis');
const connectRedis = require('connect-redis');
const Pool = require('pg-pool');

const FOLDER_PUBLIC = path.join(__dirname, 'public');
const FOLDER_AVATAR = path.join(FOLDER_PUBLIC, 'avatars');
const SECRET = 'a9e1e596-8c34-4d3b-a3b7-0a769da6d5ad';

const app = express();
const server = http.createServer(app);
const expressCookieParser = cookieParser(SECRET);
const io = socket(server);
const db = new Pool({
    database                : 'postgres',
    host                    : 'localhost',
    password                : '12345',
    port                    : '5432',
    user                    : 'postgres',
    connectionTimeoutMillis : 1000,
    idleTimeoutMillis       : 300000,
    max                     : 20,
});
const RedisStore = connectRedis(session);
const redisClient = redis.createClient();
const sessionStore = new RedisStore({ client: redisClient });
const sessionMiddleware = session({
    genid             : () => uuidv4(),
    store             : sessionStore,
    resave            : false,
    saveUninitialized : true,
    secret            : SECRET,
    cookie            : {
        maxAge: 256000 * 10000,
    },
    key: 'chat_cookie',
});
const availableParam = function availableParam(obj, ...params) {
    return obj && params && params.length !== 0 && params.every((p) => p in obj);
};

const sessionCheck = (req, res, next) => {
    if ((req.session.person && req.cookies.chat_cookie)) {
        next();
    } else {
        res.redirect('/signin');
    }
}

const isEmptyString = /^\s*$/;

app.use(compress());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(sessionMiddleware);
app.use(expressCookieParser);

app.use('/js', express.static(path.join(FOLDER_PUBLIC, 'js')));
app.use('/css', express.static(path.join(FOLDER_PUBLIC, 'css')));
app.use('/fonts', express.static(path.join(FOLDER_PUBLIC, 'fonts')));
app.use('/assets', express.static(path.join(FOLDER_PUBLIC, 'assets')));
app.use('/font-awesome', express.static(path.join(FOLDER_PUBLIC, 'font-awesome')));

const connectedSockets = [];
io.on('connect', (sckt) => {
    expressCookieParser(sckt.handshake, {}, () => {
        sessionStore.get(sckt.handshake.signedCookies['chat_cookie'], (err, sessionData) => {
            if (sessionData != null) sckt.session = sessionData;
            connectedSockets.push(sckt);
        });
    });
    sckt.on('signout', () => {
        connectedSockets.splice(connectedSockets.indexOf(sckt), 1);
    });
});

app.get('/', sessionCheck, (req, res) => {
    res.sendFile(path.join(FOLDER_PUBLIC, 'index.html'));
});
const ico = path.join(FOLDER_PUBLIC, 'favicon.ico');
app.get('/favicon.ico', (req, res) => {
    fs
        .promises
        .access(ico, fs.constants.R_OK)
        .then(() => fs.createReadStream(ico).pipe(res))
        .catch(() => res.status(404).send('Файл не найден'));
});

app.post('/getstats', sessionCheck, async (req, res) => {
    const stats = (await db.query(`
        SELECT p.name, COUNT(m.id) AS count
        FROM (chat.room r LEFT JOIN chat.message m ON r.id = m.room_id)
            JOIN chat.person p ON ((r.mate = p.id OR r.owner = p.id) AND NOT p.id = $1)
        WHERE r.owner = $1
        OR r.mate = $1
        GROUP BY p.name
    `, [req.session.person.id])).rows;

    res.send(stats);
});

app.post('/addroom', sessionCheck, async (req, res) => {
    const room = (await db.query(`
        INSERT INTO chat.room(owner, mate)
        VALUES ($1, $2)
        RETURNING id
    `, [req.session.person.id, req.body.mate])).rows[0];

    const person = (await db.query(`
        SELECT name, avatar FROM chat.person WHERE id = $1
    `, [req.body.mate])).rows[0]

    room.name = person.name;
    room.avatar = person.avatar;

    res.send(room);
})

app.post('/getavailablecontacts', sessionCheck, async (req, res) => {
    const contacts = (await db.query(`
        SELECT p.id, p.name, p.avatar
        FROM chat.person p
        WHERE p.id NOT IN (SELECT mate FROM chat.room r WHERE r.owner = $1)
              AND NOT p.id = $1
        `, [req.session.person.id])).rows;
    res.send(contacts);
});

app.post('/sentmessage', sessionCheck, async (req, res) => {
    if (!availableParam(req.body, 'text', 'room_id') || isEmptyString.test(req.body.text)) {
        return res.send('Не верно указан параметр');
    }

    const message = (await db.query(`
        INSERT INTO chat.message(text, room_id, owner)
        VALUES($1, $2, $3)
        RETURNING id, text, timestamp AS time, room_id, owner`,
        [
            req.body.text,
            req.body.room_id,
            req.session.person.id
        ])).rows[0];

    const room = (await db.query(`
        SELECT owner, mate
        FROM chat.room
        WHERE id = $1
     `, [message.room_id])).rows[0];

    message.person_sender = (await db.query(`SELECT name, avatar FROM chat.person WHERE id = $1`, [req.session.person.id])).rows[0]

    connectedSockets.forEach((sckt) => {
        const socket_person_id = sckt.session && sckt.session.person && sckt.session.person.id;
        if (socket_person_id === room.owner || socket_person_id === room.mate) {
            message.my_message = socket_person_id === req.session.person.id;

            io.to(sckt.id).emit('sentmessage', message);
        }
    });

    return res.status(200).send('Ok.');
});

app.route('/signin')
    .get((req, res) => {
        return res.sendFile(path.join(FOLDER_PUBLIC, 'signin.html'));
    })
    .post(async (req, res) => {
        if (!availableParam(req.body, 'name', 'pwd')) {
            return res.status(400).send('Не верно указаны параметры');
        }
        try {
            const person = (await db.query(`
                SELECT id, name
                FROM chat.person
                WHERE name = $1 AND password = $2`,
                [
                    req.body.name,
                    req.body.pwd
                ])).rows[0];

            if (person) {
                req.session.person = {
                    id   : person.id,
                    name : person.name
                }
                res.redirect('/');
            } else {
                res.send('Пользователя с такими учетными данными не существует');
            }

        } catch (error) {
            console.log(error);
            res.send('Не удалось авторизоваться');
        }
    })

app.route('/signup')
    .get((req, res) => {
        res.sendFile(path.join(FOLDER_PUBLIC, 'signup.html'));
    })
    .post(async (req, res) => {
        if (!availableParam(req.body, 'name', 'pwd_1', 'pwd_2')) {
            return res.status(400).send('Не верно указаны параметры');
        } else if (req.body.pwd_1 !== req.body.pwd_2) {
            return res.status(400).send('Пароли не совпадают');
        }
        const id = await db.query('INSERT INTO chat.person(name, password) VALUES ($1, $2) RETURNING id',
            [req.body.name, req.body.pwd_1]);
        res.redirect('/signin');
    });

app.post('/signout', (req, res) => {
    req.session.person = null;
    req.cookies.chat_cookie = null;
    res.send('Ok');
});

app.post('/getmessages', sessionCheck, async (req, res) => {
    const mate_room = (await db.query(`
        SELECT p.name, p.avatar
        FROM chat.person p JOIN chat.room r ON p.id = r.mate
        WHERE r.id = $1
    `, [req.body.room_id])).rows[0];
    const messages = (await db.query(`
        SELECT m.id,
            m.text,
            m.timestamp AS time,
            m.owner = $1 AS my_message,
            (SELECT JSONB_BUILD_OBJECT(
                    'name', p.name,
                    'avatar', p.avatar)
            FROM chat.person p
            WHERE p.id = m.owner)                           AS person_sender
        FROM chat.message m
            JOIN chat.room r ON r.id = m.room_id
        WHERE r.id = $2
        ORDER BY m.timestamp
    `, [req.session.person.id, req.body.room_id])).rows;

    const data = {
        mate_name   : mate_room.name,
        mate_avatar : mate_room.avatar,
        messages    : messages
    };

    res.send(data);
})

app.post('/getchat', sessionCheck, async (req, res) => {
    const user = (await db.query('SELECT id, name, avatar FROM chat.person WHERE id = $1',
        [req.session.person.id])).rows[0];

    const contacts = (await db.query(`
        SELECT r.id    AS room_id,
            (SELECT JSON_BUILD_OBJECT(
                    'person_id', p.id,
                    'name', p.name,
                    'avatar', p.avatar
                    )
            FROM chat.person p
            WHERE p.id = (
            CASE WHEN r.owner = $1 THEN
                    r.mate
                    ELSE
                    r.owner
            END))    AS person,
            (SELECT text FROM chat.message m WHERE m.room_id = r.id ORDER BY m.timestamp DESC LIMIT 1) AS last_msg
        FROM chat.room r
        WHERE r.owner = $1
        OR r.mate = $1`,
        [req.session.person.id])).rows;

    const data = {
        user     : user,
        contacts : contacts,
    };

    res.send(data);
});

app.get('/getavatar/:link', sessionCheck, (req, res) => {
    if (!availableParam(req.params, 'link')) {
        return res.send('Не верный параметр');
    }
    let file = path.join(FOLDER_AVATAR, req.params.link);
    fs
        .promises
        .access(file, fs.constants.R_OK)
        .then(() => fs.createReadStream(file).pipe(res))
        .catch(() => res.status(404).send('Файл не найден'));
})

app.use((req, res) => {
    res.status(404).send('Ресурс не найден [File not found]');
});

const port = '3000';
const hostname = '127.0.0.1';
server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});