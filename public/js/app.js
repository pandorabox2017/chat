(function () {
  const socket = io();
  const status_online = {
    '0' : ['online', 'Онлайн'],
    '1' : ['away', 'Отошёл'],
    '2' : ['busy', 'Не беспокоить'],
    '3' : ['offline', 'Оффлайн'],
  }
  function generateRandomColor()
  {
    var randomColor = '#'+Math.floor(Math.random()*16777215).toString(16);
    if(randomColor.length != 7){ // In any case, the color code is invalid
        randomColor = generateRandomColor();
    }
    return randomColor;
  }

  Vue.component('my-chart', {
    extends : VueChartJs.Pie,
    props   : ['data', 'options'],
    watch   : {
      data: function (val) {
        this.renderChart({
          labels   : val.map(x => x.name),
          datasets : [
            {
              label           : 'Data One',
              backgroundColor : val.map(x => generateRandomColor()),
              data            : val.map(x => x.count)
            }
          ]
        },
        {
          responsive          : true,
          maintainAspectRatio : false,
          legend              : {
            display : true,
            labels  : {
              fontColor: '#f5f5f5'
            }
          }
        })
      }
    },
  })

  // eslint-disable-next-line no-unused-vars
  const app = new Vue({
    el   : '#app',
    data : {
      user: {
        name   : null,
        avatar : null,
      },
      contacts      : [],
      persons       : [],
      show_content  : false,
      popup_contact : false,
      popup_stats   : false,
      message_text  : '',
      content       : {
        mate_name   : null,
        mate_avatar : null,
        room_id     : null,
        messages    : []
      },
      stat_data: [],
    },
    methods: {
      show_stats: function () {
        $.ajax({
          type    : 'POST',
          url     : '/getstats',
          success : (data) => {
            this.stat_data = data;
            this.popup_stats = true;
          }
        })
      },
      append_room: function (id) {
        $.ajax({
          type : 'POST',
          url  : '/addroom',
          data : {
            mate: id
          },
          success: (c) => {
            this.contacts.push({
              room_id        : c.id,
              user_name      : c.name,
              avatar         : c.avatar,
              last_msg       : null,
              is_active      : false,
              contact_status : status_online[0][0]
            });
            this.popup_contact = false;
          }
        })
      },
      add_contacts: function () {
        $.ajax({
          type    : 'POST',
          url     : '/getavailablecontacts',
          success : (contacts) => {
            this.persons = contacts;
            this.popup_contact = true;
          }
        })
        this.popup_contact = true
      },
      sent_message: function () {
        $.ajax({
          type : 'POST',
          url  : '/sentmessage',
          data : {
            text    : this.message_text,
            room_id : this.content.room_id,
          },
          success: () => {
            this.message_text = '';
          }
        })
      },
      signout: () => {
        $.ajax({
          type    : 'POST',
          url     : '/signout',
          success : () => {
            socket.emit('signout');
            window.location.reload();
          }
        })
      },
      activate_chat: function (contact) {
        this.contacts.forEach(c => c.is_active = false);
        $.ajax({
          type : 'POST',
          url  : '/getmessages',
          data : {
            room_id: contact.room_id
          },
          success: (content) => {
            this.show_content = false;
            this.content.messages.splice(0);
            setTimeout(() => {
              this.content.room_id = contact.room_id;
              this.content.mate_name = content.mate_name;
              this.content.mate_avatar = content.mate_avatar,
                content.messages.forEach(m => {
                  this.content.messages.push({
                    id            : m.id,
                    text          : m.text,
                    time          : moment(m.time).format("HH:mm"),
                    my_message    : m.my_message,
                    person_sender : m.person_sender
                  });
                });
              // this.content = content;
              contact.is_active = true;
              this.show_content = true;
            }, 300);
          }
        })

      }
    },
    mounted: function () {
      socket.on('sentmessage', message => {
        if (this.content.room_id === message.room_id) {
          this.content.messages.push({
            id            : message.id,
            text          : message.text,
            time          : moment(message.time).format("HH:mm"),
            my_message    : message.my_message,
            person_sender : message.person_sender
          });
        }
        const contact = this.contacts.filter(c => c.room_id === message.room_id)[0];
        if (contact) {
          contact.last_msg = message.text;
        } else {
          this.contacts.push({
            room_id        : message.room_id,
            user_name      : message.person_sender.name,
            avatar         : message.person_sender.avatar,
            last_msg       : message.text,
            is_active      : false,
            contact_status : status_online[0][0]
          })
        }
      });
    },
    beforeCreate: function () {
      $.ajax({
        type    : 'POST',
        url     : '/getchat',
        success : (data) => {
          this.user.name = data.user.name;
          this.user.avatar = data.user.avatar;
          data.contacts.forEach(c => {
            this.contacts.push({
              room_id        : c.room_id,
              user_name      : c.person.name,
              avatar         : c.person.avatar,
              last_msg       : c.last_msg,
              is_active      : false,
              contact_status : status_online[0][0]
            })
          });
        }
      });
    }
  })
})()